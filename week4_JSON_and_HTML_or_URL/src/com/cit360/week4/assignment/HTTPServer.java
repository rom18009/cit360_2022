package com.cit360.week4.assignment;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;

public class HTTPServer {
    // Main method + IOException
    public static void main(String[] args) throws IOException {
        // Initializing the localhost portS
        final int PORT = 7501;

        HttpServer httpServer = HttpServer.create(new InetSocketAddress(PORT),0);
        httpServer.createContext("/trucks", new TruckRequestHandler());
        System.out.println("Server is now started \n Port: " + PORT);
        httpServer.setExecutor(null);
        httpServer.start();
    }
}
