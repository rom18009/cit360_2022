package com.cit360.week4.assignment;

public class Truck {
    private String name;
    private String type;

    public Truck() {}

    // Constructor
    public Truck(String name, String type) {
        this.name = name;
        this.type = type;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    // Info for print out
    public String toString() {
        // Returns name and model of trucks
        return "Make: " + type + " Model: " + name;
    }
}
